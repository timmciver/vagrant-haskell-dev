sudo apt update

# install git
sudo apt install -y git

# install emacs
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update
sudo apt install -y emacs26-nox

# install stack
if ! type "$stack" > /dev/null; then
    curl -sSL https://get.haskellstack.org/ | sh
fi
